#!/bin/bash

ISO_ARCH="x86_64-musl x86_64"
ISO_EDITION="studio home"
ISO_IMAGES="amd amd-nvidia intel intel-nvidia"
ISO_IMAGES_MUSL="amd amd-nouveau intel intel-nouveau"

for ARCH in $ISO_ARCH; do
  for EDITION in $ISO_EDITION; do

    if [ "$ARCH" == "x86_64" ]; then
      VARIANTS=$ISO_IMAGES
    elif [ "$ARCH" == "x86_64-musl" ]; then
      VARIANTS=$ISO_IMAGES_MUSL
    fi

    for VARIANT in $VARIANTS; do
      # echo "./build-x86-images.sh -a $ARCH -b $EDITION-$VARIANT"
      ./build-x86-images.sh -a $ARCH -b $EDITION-$VARIANT
    done

  done
done
